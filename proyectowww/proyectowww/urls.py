"""proyectowww URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^usuarios/', include('apps.usuario.urls')),
    url(r'^eventos/', include('apps.eventos.urls')),
    url(r'^noticias/', include('apps.noticias.urls')),
    url(r'^actividad/',include('apps.actividad.urls')),

    url(r'^',include('apps.landing_page.urls')),
    url(r'^reporte/',include('apps.reporte.urls')),
    url(r'^select2/', include('django_select2.urls')),

]+ static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
