function tipo_reporte() {

    var tipo = null;
    var tipo_grafico = 'barras'//$('#seleccion-tipo-grafico').val();
    if (tipo_grafico == 'barras') {
        tipo = 'bar';
    } else if (tipo_grafico == 'barras_horizontales') {
        tipo = 'horizontalBar';
    }
    return tipo;
}

function generar_reporte_barras_restriccion_fecha(url_ajax, panel_grafico, selector_fecha, detalle) {
    var tipo = tipo_reporte();
    var fecha_inicio = $(selector_fecha).data('daterangepicker').startDate.format("YYYY-MM-DD HH:mm:ssZZ");
    var fecha_fin = $(selector_fecha).data('daterangepicker').endDate.format("YYYY-MM-DD HH:mm:ssZZ");

    $.blockUI({
        message: '<h1 style="color:#ffffff !important;"> Generando reporte ...</h1>',
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#177bbb',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#ffffff'
        }
    });
    $.ajax({
        type: "GET",
        url: url_ajax,
        dataType: 'json',
        data: {
            'fecha_inicio': fecha_inicio,
            'fecha_fin': fecha_fin,
            'detalle': detalle
        },
        success: function (data) {

            $('.data-combine').DataTable().clear();
            $('.data-combine').DataTable().rows.add(data.datos_tabla);
            $('.data-combine').DataTable().draw();



            var barChartData = {
                labels: data.etiquetas,
                datasets: [{
                    label: "Asistencia",
                    borderWidth: 2,
                    backgroundColor: data.colores,
                    borderColor: data.colores,
                    data: data.valores
                }]
            };
            $('#contenedor-grafico').empty();
            $('#contenedor-grafico').append('<canvas id=' + panel_grafico + '></canvas>');
            var contexto = document.getElementById(panel_grafico).getContext('2d');
            var titulo_ejeY = '';
            var titulo_ejeX = '';
            var opciones_scale = {};
            if (tipo == 'bar') {
                titulo_ejeY = data.ejeY;
                titulo_ejeX = data.ejeX;
                opciones_scale = {
                        xAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: titulo_ejeX
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                min: 0,
                                max: 100,
                                callback: function (value) {
                                    return value + "%"
                                },
                                maxTicksLimit: 100
                            },
                            scaleLabel: {
                                display: true,
                                labelString: titulo_ejeY
                            }
                        }]
                    }

            } /*else if (tipo == 'horizontalBar') {
                // Con las barras horizontales se deben intercambiar las etiquetas de los ejes
                titulo_ejeY = data.ejeX;
                titulo_ejeX = data.ejeY;
                opciones_scale = {
                        xAxes: [{
                            ticks: {
                                min: 0,
                                max: 100,
                                callback: function (value) {
                                    return value + "%"
                                },
                                maxTicksLimit: 100
                            },
                            scaleLabel: {
                                display: true,
                                labelString: titulo_ejeX
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                maxTicksLimit: 100
                            },
                            scaleLabel: {
                                display: true,
                                labelString: titulo_ejeY
                            },
                        }]
                    }

            }*/
            var barChart = new Chart(contexto, {
                type: tipo,
                data: barChartData,

                options: {
                    title: {
                        display: true,
                        text: 'Porcentaje de asistencia por fecha'
                    },
                    legend: {
                        display: false
                    },
                    scales: opciones_scale
                }
            });
            $.unblockUI();
        }
    });
}


function generar_reporte_fecha(boton_actualizar, url_ajax, panel_grafico, selector_fecha) {

    var funcion_reporte_a_usar = null;
    var tipo = null;
    var tipo_grafico = 'barras' //$('#seleccion-tipo-grafico').val();
    var detalle = 'dia' //$('#seleccion-detalle-fecha').val().funcion_reporte_a_usar = generar_reporte_barras_restriccion_fecha;

    if (tipo_grafico == 'barras') {
        funcion_reporte_a_usar = generar_reporte_barras_restriccion_fecha;
        tipo = 'bar'
    } else if (tipo_grafico == 'barras_horizontales') {
        funcion_reporte_a_usar = generar_reporte_barras_restriccion_fecha;
        tipo = 'horizontalBar'
    }
    $(boton_actualizar).on('click', function () {

        funcion_reporte_a_usar(url_ajax, panel_grafico, selector_fecha, tipo, detalle);

    });
    funcion_reporte_a_usar(url_ajax, panel_grafico, selector_fecha, tipo, detalle);

    /*
     'a[data-toggle="tab"]' son los elementos 'a' con data-toogle = 'tab'
     'shown.bs.tab' es el evento generado cuando la pestaña se muestra
     */
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        funcion_reporte_a_usar(url_ajax, panel_grafico, selector_fecha, tipo, detalle);

    });
}
