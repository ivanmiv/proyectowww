
function generar_reporte_barras(url_ajax, panel_grafico, selector_evento, detalle) {
    var evento = detalle;
    var tipo = 'bar';
    $.blockUI({
        message: '<h1 style="color:#ffffff !important;"> Generando reporte ...</h1>',
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#177bbb',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#ffffff'
        }
    });
    $.ajax({
        type: "GET",
        url: url_ajax,
        dataType: 'json',
        data: {
            'evento': evento
        },
        success: function (data) {

            $('.data-combine').DataTable().clear();
            $('.data-combine').DataTable().rows.add(data.datos_tabla);
            $('.data-combine').DataTable().draw();



            var barChartData = {
                labels: data.etiquetas,
                datasets: [{
                    label: "Asistencia",
                    borderWidth: 2,
                    backgroundColor: data.colores,
                    borderColor: data.colores,
                    data: data.valores
                }]
            };
            $('#contenedor-grafico').empty();
            $('#contenedor-grafico').append('<canvas id=' + panel_grafico + '></canvas>');
            var contexto = document.getElementById(panel_grafico).getContext('2d');
            var titulo_ejeY = '';
            var titulo_ejeX = '';
            var opciones_scale = {};
            if (tipo == 'bar') {
                titulo_ejeY = data.ejeY;
                titulo_ejeX = data.ejeX;
                opciones_scale = {
                        xAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: titulo_ejeX
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                min: 0,
                                max: data.maximo + 5,
                                callback: function (value) {
                                    return value 
                                },
                                maxTicksLimit: data.maximo + 5
                            },
                            scaleLabel: {
                                display: true,
                                labelString: titulo_ejeY
                            }
                        }]
                    }

            } 
            var barChart = new Chart(contexto, {
                type: tipo,
                data: barChartData,

                options: {
                    title: {
                        display: true,
                        text: 'Asistencia por actividad'
                    },
                    legend: {
                        display: false
                    },
                    scales: opciones_scale
                }
            });
            $.unblockUI();
        }
    });
}

function generar_reporte_cantidad_asistentes(boton_actualizar, url_ajax, panel_grafico, selector_evento) {
//function generar_reporte_barras(url_ajax, panel_grafico, selector_evento, detalle)
    var funcion_reporte_a_usar = null;
    var evento = $('#selector_evento').val();

    funcion_reporte_a_usar = generar_reporte_barras;

    $(boton_actualizar).on('click', function () {
        var evento = $('#selector_evento').val();
        funcion_reporte_a_usar(url_ajax, panel_grafico, selector_evento, evento);

    });
    funcion_reporte_a_usar(url_ajax, panel_grafico, selector_evento, evento);

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var evento = $('#selector_evento').val();
        funcion_reporte_a_usar(url_ajax, panel_grafico, selector_evento, evento);

    });
}