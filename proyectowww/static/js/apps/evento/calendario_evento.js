function actualizarCalendario(url_ajax,id_evento) {
    $('#actualizar').on('click',function () {
        visualizar_actividades_evento(url_ajax,id_evento)
    });
    visualizar_actividades_evento(url_ajax,id_evento);
}

function visualizar_actividades_evento(url_ajax,id_evento) {


    $.ajax({
        type: "GET",
        url: url_ajax,
        dataType: 'json',
        data: {
            "pk_evento": id_evento
        },
        success: function (datos) {

            $('.calendar').fullCalendar({
                locale:'es',
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                editable: false,
                defaultDate: moment().format("YYYY-MM-DD"),
                eventLimit: true // allow "more" link when too many events
            });
            $('.calendar').fullCalendar('removeEvents');

            var actividades = datos.datos;
            for (actividad in actividades) {

                var programacion = {
                    title: actividades[actividad].titulo,
                    start: actividades[actividad].fechaInicio,
                    end: actividades[actividad].fechaFin,
                    color: actividades[actividad].color
                };
                $('.calendar').fullCalendar('renderEvent', programacion, 'stick');
            }


        }
    });
}