from django.contrib.auth.models import AbstractUser
from django.db import models


def crear_ruta_foto_perfil(instance, filename):
    return "fotos-perfil/%s-%s"%(instance.id, filename.encode('ascii','ignore'))


class Usuario(AbstractUser):

    CARGOS=(
        ('Administrador','Administrador'),
        ('Gerente', 'Gerente'),
        ('Operador', 'Operador'),
    )

    cargo = models.CharField(max_length=20,choices=CARGOS)
    foto_perfil = models.ImageField(upload_to=crear_ruta_foto_perfil, blank=True, help_text='Seleccione una imagen de perfil')


    @staticmethod
    def crear_usuario_inicial():
        total_usuarios = Usuario.objects.all().count()
        if total_usuarios == 0:
            password = "Cedesoft"
            user = Usuario.objects.create_user('admin', 'root@gmail.com', password)
            user.set_password(password)
            user.first_name = 'Administrador'
            user.is_superuser = True
            user.is_staff = True
            user.cargo = "Administrador"
            user.save()

    @staticmethod
    def revisar_existencia_usuario(usuario):
        try:
            usuario = Usuario.objects.get(id=usuario)
            return usuario
        except Exception:
            pass
        return False

    def __str__(self):
        return "%s %s"%(self.first_name,self.last_name)

