from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.urls.base import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import  ListView
from proyectowww.utilidades import verificar_cargo
from .forms import *

from .models import Usuario
from django.views.generic import FormView

from .forms import CrearUsuarioForm


def inicio(request):
    if request.user.cargo == "Administrador":
        return redirect('usuario_listar')

    return redirect('evento_listar')


@verificar_cargo(cargos_permitidos=["Administrador"])
def crear_modificar_usuario(request, id_usuario=None):
    usuario = Usuario.revisar_existencia_usuario(id_usuario)
    clase_form = ModificarUsuarioForm
    titulo = "Modificar usuario"
    if not usuario:
        if id_usuario:
            messages.error(request, "No existe el usuario")
            return redirect('usuario_listar')

        usuario = None
        clase_form = CrearUsuarioForm
        titulo = "Registrar usuario"

    if request.method == 'POST':
        form = clase_form(request.POST,request.FILES,instance = usuario)

        if form.is_valid():
            form.save()

            messages.success(request, "El usuario ha sido guardado correctamente")
            return redirect('usuario_listar')

        messages.error(request, "Error al guardar la información del usuario")

    else:
        form = clase_form(instance=usuario)

    return render(request,"crear_modificar_usuario.html",{'form':form, 'titulo_panel':titulo})


@verificar_cargo(cargos_permitidos=["Administrador","Operador","Gerente"])
def modificar_datos_personales_usuario(request, id_usuario):
    usuario = Usuario.revisar_existencia_usuario(id_usuario)
    titulo = "Modificar datos personales usuario"
    if not usuario:
        messages.error(request, "No existe el usuario")
        return redirect(request.META.get('HTTP_REFERER','evento_listar'))
    if usuario != request.user:
        messages.error(request, "No es posible modificar la información del usuario")
        return redirect(request.META.get('HTTP_REFERER', 'evento_listar'))

    if request.method == 'POST':
        form = ModificarDatosPersonalesUsuarioForm(request.POST, request.FILES, instance=usuario)

        if form.is_valid():
            form.save()

            messages.success(request, "Se ha guardado el usuario correctamente")
            return redirect(request.META.get('HTTP_REFERER', 'evento_listar'))

        messages.error(request, "Error al guardar el usuario")

    else:
        form = ModificarDatosPersonalesUsuarioForm(instance=usuario)

    return render(request, "modificar_datos_personales_usuario.html", {'form': form, 'titulo_panel': titulo})


@verificar_cargo(cargos_permitidos=["Administrador"])
def activar_usuario(request, id_usuario):
    try:
        usuario = Usuario.objects.get(id=id_usuario)
        usuario.is_active=True
        usuario.save()
        messages.success(request, "El usuario ha sido activado correctamente")
    except Usuario.DoesNotExist:
        messages.error(request, "El usuario solicitado no existe")
    return redirect('usuario_listar')


@verificar_cargo(cargos_permitidos=["Administrador"])
def desactivar_usuario(request, id_usuario):
    try:
        usuario = Usuario.objects.get(id=id_usuario)
        usuario.is_active=False
        usuario.save()
        messages.success(request, "El usuario ha sido desactivado correctamente")
    except Usuario.DoesNotExist:
        messages.error(request, "El usuario solicitado no existe")
    return redirect('usuario_listar')


class ListarUsuarios(LoginRequiredMixin, ListView):
    model = Usuario
    template_name = 'lista_usuarios.html'
    queryset = Usuario.objects.all()
    context_object_name = 'lista_usuarios'
    login_url = reverse_lazy('login')

    @verificar_cargo(cargos_permitidos=["Administrador"])
    def dispatch(self, request, *args, **kwargs):
        return super(ListarUsuarios, self).dispatch(request,*args,**kwargs)


class Login(FormView):
    form_class = LoginForm
    template_name = 'login.html'

    def dispatch(self, request, *args, **kwargs):
        return super(Login, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        mensaje = ""
        user = authenticate(username = form.cleaned_data['username'], password = form.cleaned_data['password'])
        if user is not None:
            if user.is_active:
                self.success_url = reverse_lazy('evento_listar')
                login(self.request, user)
                usuario = get_object_or_404(Usuario, pk=self.request.user.pk)
                usuario.save()

                if usuario.cargo=="Administrador":
                    return redirect('usuario_listar')

                return super(Login, self).form_valid(form)
            else:
                mensaje = "El usuario  o la contraseña son incorrectos"
        else:
            mensaje = "El usuario  o la contraseña son incorrectos"
        form.add_error('username', mensaje)
        messages.add_message(self.request, messages.ERROR, mensaje)
        return super(Login, self).form_invalid(form)

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR, "El usuario  o la contraseña son incorrectos")
        return super(Login, self).form_invalid(form)


@login_required
def Logout(request):
    logout(request)
    return redirect('login')


@verificar_cargo(cargos_permitidos=["Administrador","Gerente","Operador"])
def restablecer_contrasena(request,id_usuario):
    usuario = Usuario.revisar_existencia_usuario(id_usuario)

    if not usuario:
        messages.error(request, "El usuario solicitado no existe")
        return redirect(request.META.get('HTTP_REFERER','evento_listar'))

    if request.user.cargo != "Administrador" and request.user != usuario:
        messages.error(request, "No tiene permiso para modificar la información del usuario")
        return redirect('evento_listar')

    if request.method == 'POST':
        form = RestablecerContrasenaForm(request.POST)

        if form.is_valid():
            password = form.cleaned_data['password1']
            usuario.set_password(password)
            usuario.save()
            messages.success(request, "Se ha editado exitosamente la contraseña del usuario")
            return redirect('usuario_listar')
        else:
            messages.error(request, "Ha ocurrido un erro al editar la contraseña del usuario")
    else:
        form = RestablecerContrasenaForm()

    contexto={
        'form':form,
        'usuario':usuario
    }

    return render(request,"restablecer_contrasena.html",contexto)
