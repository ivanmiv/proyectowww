from captcha.fields import ReCaptchaField
from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import Usuario

class CrearUsuarioForm(UserCreationForm):

    def __init__(self, *args, **kwargs):
        super(CrearUsuarioForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].label = 'Nombres'
        self.fields['last_name'].label = 'Apellidos'
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True

    username = forms.IntegerField(required=True, label="Número de documento de identificación",
                                  error_messages={'unique': "Ya existe un usuario con este número de documento de identificación" ,
        })
    password1 = forms.CharField(max_length=30, required=True, label="Contraseña", widget=forms.PasswordInput)

    class Meta:
        model = Usuario
        fields = ('first_name', 'last_name', 'email', 'username', 'cargo')



class ModificarUsuarioForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ModificarUsuarioForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].label = 'Nombres'
        self.fields['last_name'].label = 'Apellidos'
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True

    username = forms.IntegerField(required=True, label="Número de documento de identificación")

    class Meta:
        model = Usuario
        fields = ('first_name', 'last_name', 'email', 'username', 'cargo','foto_perfil')



class ModificarDatosPersonalesUsuarioForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ModificarDatosPersonalesUsuarioForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].label = 'Nombres'
        self.fields['last_name'].label = 'Apellidos'
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True

    username = forms.IntegerField(required=True, label="Número de documento de identificación")

    class Meta:
        model = Usuario
        fields = ('first_name', 'last_name', 'email', 'username','foto_perfil')




class LoginForm(forms.Form):

    username = forms.CharField(max_length = 50,
            widget=forms.TextInput(attrs ={
                    'id':'usernameInput',
                    'placeholder': 'Nombre de usuario',
                    'class':'form-control'
                }))
    password = forms.CharField(max_length = 50,
            widget = forms.TextInput(attrs = {
                    'type' : 'password',
                    'id':'passwordInput',
                    'placeholder': 'Contraseña',
                    'class':'form-control'
                }))

    confirmacion =  ReCaptchaField(error_messages={'required': "Por favor realice la confirmación mediante captcha"})



class RestablecerContrasenaForm(forms.Form):
    password1 = forms.CharField(label='Contraseña nueva',max_length = 50,
            widget = forms.TextInput(attrs = {
                    'type' : 'password',
                    'placeholder': 'Contraseña',
                    'data-parsley-contrasena':'',
                    'class':'form-control'
                }))

    password2 = forms.CharField(label='Confirmar contraseña nueva',max_length = 50,
            widget = forms.TextInput(attrs = {
                    'type' : 'password',
                    'placeholder': 'Confirmar contraseña',
                    'data-parsley-confirmacion':'id_password1',
                    'class':'form-control'
                }))

    def clean(self):
        form_data = self.cleaned_data        
        if form_data["password1"] != form_data["password2"]:
            self._errors["password2"] = ["Las contraseñas no coinciden"]
        return form_data
