from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^crear$', crear_modificar_usuario, name ='usuario_crear_modificar'),
    url(r'^modificar/(?P<id_usuario>\d+)$', crear_modificar_usuario, name ='usuario_crear_modificar'),
    url(r'^modificar_datos_personales/(?P<id_usuario>\d+)$', modificar_datos_personales_usuario, name ='usuario_modificar_datos_personales'),
    url(r'listar$', ListarUsuarios.as_view(), name = 'usuario_listar'),

    url(r'desactivar/(?P<id_usuario>\d+)$', desactivar_usuario, name='usuario_desactivar'),
    url(r'activar/(?P<id_usuario>\d+)$', activar_usuario, name = 'usuario_activar'),
    url(r'^login$',Login.as_view() ,name = 'login'),
    url(r'^logout$',Logout ,name = 'logout'),
    url(r'^restablecer-contrasena/(?P<id_usuario>\d+)$', restablecer_contrasena, name="usuarios_restablecer_contrasena"),

]
