from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^crear$', crear_evento, name ='crear_evento'),
    url(r'^modificar/(?P<id_evento>\d+)$', modificar_evento, name ='evento_modificar'),
    url(r'^listar$', ListarEventos.as_view(), name = 'evento_listar'),

    url(r'^desactivar/(?P<id_evento>\d+)$', desactivar_evento, name='evento_desactivar'),
    url(r'^activar/(?P<id_evento>\d+)$', activar_evento, name = 'evento_activar'),

    url(r'^ver/inscripcion/(?P<evento_id>[0-9]+)$', inscribir_en_evento, name='inscribir_en_evento'),
    url(r'^ver/escarapela/(?P<evento_id>[0-9]+)/(?P<participante_id>[0-9]+)$', crear_escarapela, name= 'crear_escarapela'),
    url(r'^ver/(?P<evento_id>[0-9]+)/pre_inscripcion$', pre_inscripcion, name ='evento_pre_inscripcion'),
    url(r'^ver/(?P<evento_id>[0-9]+)/$', detalles_evento, name = 'detalles_evento'),
    url(r'^ver/(?P<evento_id>[0-9]+)/calendario$', CalendarioEvento, name = 'calendario_evento'),
    url(r'^calendario/datos$', datos_calendario, name = 'evento_datos_calendario'),

    url(r'^preinscrito/(?P<evento_id>[0-9]+)/listado$', listar_preinscrito, name='listar_preinscrito'),
    url(r'^participante/codigo', participante_ingresar_codigo, name='participante_codigo_participante'),
    url(r'^participante/modificar$', participante_modificar_datos_personales, name= 'participante_modificar'),
    url(r'^participante/modificar/(?P<evento_id>\d+)/(?P<participante_id>\d+)$', participante_modificar, name= 'participante_modificar'),
    url(r'^preinscrito/aceptar/(?P<evento_id>\d+)/(?P<inscripcion_id>\d+)$', aceptar_preinscrito, name= 'aceptar_preinscrito'),
    url(r'^preinscrito/rechazar/(?P<evento_id>\d+)/(?P<inscripcion_id>\d+)$', rechazar_preinscrito, name= 'rechazar_preinscrito'),

    url(r'^recuperar-codigo', recuperar_codigo, name='evento_recuperar_codigo'),
]