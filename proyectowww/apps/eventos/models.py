from django.db import models
from django.urls import reverse_lazy

from proyectowww.utilidades import enviar_email

def crear_ruta_evento(instance, filename):
    return "fotos-evento/%s-%s"%(instance.id, filename.encode('ascii','ignore'))



class Evento(models.Model):

    ESTADOS = (
        ('Activo', 'Activo'),
        ('Inactivo', 'Inactivo')
    )

    nombre = models.CharField(max_length=50, verbose_name="Nombre")
    descripcion = models.CharField(max_length=2000, verbose_name="Descripción")
    lugar = models.CharField(max_length=140, verbose_name="Lugar")
    direccion = models.CharField(max_length=140, verbose_name="Dirección")
    telefono = models.CharField(max_length=15, verbose_name="Teléfono")
    costo = models.PositiveIntegerField(default=1, verbose_name="Costo")
    estado = models.CharField(max_length=20, choices = ESTADOS, default='Activo', verbose_name="Estado")
    imagen = models.ImageField(upload_to=crear_ruta_evento, blank=True,help_text='Seleccione una imagen de perfil', verbose_name="Imagen")

    numero_cupos = models.PositiveIntegerField(default=1,verbose_name="Número de cupos")
    fecha_limite_inscripcion = models.DateTimeField(blank=False,null=False, verbose_name="Fecha límite de inscripción")
    fecha_inicio = models.DateTimeField(blank=False,null=False, verbose_name="Fecha inicio")
    fecha_fin = models.DateTimeField(blank=False,null=False, verbose_name="Fecha fin")


    def is_active(self):
        if(self.estado == 'Activo'):
            return True
        else:
            return False


    @staticmethod
    def revisar_existencia_evento(evento):
        try:
            evento = Evento.objects.get(id=evento)
            return evento
        except Exception:
            pass
        return False

    def __str__(self):
        return "%s (%s)"%(self.nombre,self.descripcion)

    @staticmethod
    def obtener_eventos():
        try:
            evento = Evento.objects.all()
            return evento
        except Exception as e:
            raise e
        return False

    # @staticmethod
    # def revisar_existencia_evento(nombre_evento):
    #     try:
    #         evento = Evento.objects.get(nombre=nombre_evento)
    #         return evento
    #     except Exception as e:
    #         raise e
    #     return False


class Participante(models.Model):

    nombres = models.CharField(max_length = 90, verbose_name="Nombres")
    apellidos = models.CharField(max_length = 90, verbose_name="Apellidos")
    correo = models.EmailField(max_length = 254, unique = True, verbose_name="Correo electrónico")
    numero_documento = models.CharField(max_length = 17, unique = True, verbose_name="Número de documento")
    telefono = models.CharField(max_length = 20, unique = True, verbose_name="Teléfono")


    @staticmethod
    def revisar_existencia_participante(documento):
        try:
            participante = Participante.objects.get(numero_documento = documento)
            return participante
        except Exception as e:
            return None
        return None

    @staticmethod
    def revisar_existencia_participante_id(id_participante):
        try:
            participante = Participante.objects.get(id = id_participante)
            return participante
        except Exception as e:
            return None
        return None

    @staticmethod
    def revisar_existencia_participante_correo(correo):
        try:
            participante = Participante.objects.get(correo=correo)
            return participante
        except Exception as e:
            return None
        return None

    def pre_inscribir_en_evento(self, evento_id):
        try:
            evento = Evento.revisar_existencia_evento(evento_id)
            if not evento:
                raise Exception("Evento no encontrado id %s"%evento_id)

            participacion = Inscripcion()
            participacion.participante = self
            participacion.evento = evento
            participacion.save()
            return participacion, "Usted se ha preinscrito correctamente"
        except Exception as e:
            print(e)
            return False, "Error al registrar la preinscripción"

    def obtener_codigo(self):
        codigo = (self.id * 56423) % 10012300207
        clave = "%d-%d" % (codigo, self.id)
        return clave

    @staticmethod
    def buscar_participante_codigo(codigo):
        try:
            mi_hash, mi_id  = map(int, codigo.split("-"))
            participante = Participante.objects.get(id=mi_id)
            if participante.obtener_codigo() == codigo:
                return participante
            else:
                return None
        except Exception as ex:
            return None

    def enviar_correo_recuperacion_codigo(self, request):
        participante = self

        mensaje = """
            Buen día participante %s.
            
            Ha solicitado el código para modificar sus datos personales
            
            El código para modificar sus datos personales es: %s.
        
            Puede modificar sus datos en %s

        """ % (participante, participante.obtener_codigo(), reverse_lazy('participante_codigo_participante'))

        datos_email = {
            "subject": "[Eventos ABC] Recuperación de código",
            "body": mensaje,
            "to": [participante.correo],
            "mensaje_error": "Error al enviar correo",
        }

        enviar_email(request, **datos_email)

    def __str__(self):
        return "%s %s"%(self.nombres,self.apellidos)


class Inscripcion(models.Model):
    ESTADO_INSCRIPCION = (
        ('EN ESPERA PRE', 'En espera de pre-inscripción'),
        ('PREINSCRITO', 'Preinscrito'),
        ('Rechazado', 'Rechazado'),
        ('PAGAR', 'A espera del pago'),
        ('PAGO', 'Pagado'),
        ('INSCRITO', 'Inscrito'),
    )

    participante = models.ForeignKey('Participante', related_name='eventos')
    evento = models.ForeignKey('Evento', related_name='participantes')
    estado_inscripcion = models.CharField(max_length=30, null = False, default= 'EN ESPERA PRE', choices= ESTADO_INSCRIPCION)


    @staticmethod
    def revisar_participacion(participante, evento):
        print(Inscripcion.objects.filter(participante = participante, evento = evento) is None)
        if not Inscripcion.objects.filter(participante = participante, evento = evento).exists():
            return False
        else:
            return True

    @staticmethod
    def get_participacion(participante, evento):
        try:
            participacion = Inscripcion.objects.get(participante__id = participante, evento__id = evento)
            #print(participacion)
            return participacion
        except Exception as e:
            print(e)
            return None
        return None


    def inscribir(self):
        try:
            self.estado_inscripcion = 'INSCRITO'
            self.save()
            return True
        except Exception as e:
            print(e)
            return False

    def enviar_correo_preinscripcion_aceptada(self, request):
        participante = self.participante
        evento = self.evento

        mensaje = """
            Buen día participante %s su pre-inscripción al evento %s ha sido aceptada exitosamente.

            Con el código %s puede modificar sus datos personales.
            
            Para completar su inscripción debe realizar el pago del evento en pagos.eventosabc.com.co con los siguientes datos:
            - Número de documento: %s
            - Identificador del evento: %s
            - Valor a pagar: %s
            
            Muchas gracias
        """ % (participante, evento, participante.obtener_codigo(), participante.numero_documento, evento.id, evento.costo)

        datos_email = {
            "subject": "[Eventos ABC] Pre-inscripción aceptada",
            "body": mensaje,
            "to": [participante.correo],
            "mensaje_error": "Error al enviar correo con información de la pre-inscripción aceptada, por favor informe al participante",
        }

        enviar_email(request, **datos_email)

    def enviar_correo_preinscripcion_rechazada(self, request):
        participante = self.participante
        evento = self.evento

        mensaje = """
            Buen día participante %s su pre-inscripción al evento %s ha sido rechazada.

            Muchas gracias
        """ % (participante, evento)

        datos_email = {
            "subject": "[Eventos ABC] Pre-inscripción rechazada",
            "body": mensaje,
            "to": [participante.correo],
            "mensaje_error": "Error al enviar correo con información de la pre-inscripción rechazada, por favor informe al participante",
        }
        enviar_email(request, **datos_email)


    def confirmar_pago_web_service(self, request):
        import requests
        from django.contrib import messages
        from django.conf import settings

        url_web_service = settings.URL_WEB_SERVICE
        datos = {
            'evento_id': self.evento.id,
            'numero_documento': self.participante.numero_documento,
            'valor': self.evento.costo
        }

        respuesta = requests.get(url_web_service, params=datos, verify=False)
        if respuesta.status_code == requests.codes.ok:
            estado_pago = respuesta.json()['estado_pago']
            if estado_pago == 'realizado':
                self.estado_inscripcion = 'PAGO'
                self.save()
                messages.success(request, "Pago confirmado exitosamente")
                return True
            if estado_pago == 'inexistente':
                pass

        messages.error(request, "No ha sido posible confirmar el pago")
        return False
