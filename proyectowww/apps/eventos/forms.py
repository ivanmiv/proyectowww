
from django import forms

import datetime

from django.utils import timezone

from datetimewidget.widgets import DateWidget
from datetimewidget.widgets import DateTimeWidget

from .models import Evento, Participante, Inscripcion
from proyectowww.utilidades import MyDateTimeWidget



class CrearEventoForm(forms.ModelForm):

    # fecha_limite_inscripcion = forms.DateField(label="Fecha cierre de inscripciones")
    # fecha_inicio = forms.DateField(label="Fecha de inicio")
    # fecha_fin = forms.DateField(label="Fecha de fin")



    def clean(self):
        cleaned_data = super().clean()
        fecha_inicio = cleaned_data.get('fecha_inicio')
        fecha_limite_inscripcion = cleaned_data.get('fecha_limite_inscripcion')
        fecha_fin = cleaned_data.get('fecha_fin')

        today_date = timezone.now()  # offset-awared datetime
        today_date.astimezone(timezone.utc).replace(tzinfo=None)
        if fecha_inicio:
            # print(today_date)
            # print(fecha_inicio)
            if fecha_inicio < today_date:
                msg = "La fecha de inicio no puede estar en el pasado!"
                self.add_error('fecha_inicio', msg)


        if fecha_limite_inscripcion:
            if fecha_limite_inscripcion < today_date:
                msg = "La fecha de cierre no puede estar en el pasado!"
                self.add_error('fecha_limite_inscripcion', msg)


        if fecha_fin:
            if fecha_fin < today_date:
                msg = "La fecha de fin no puede estar en el pasado!"
                self.add_error('fecha_fin', msg)


        if fecha_inicio and fecha_limite_inscripcion:
            if fecha_inicio < fecha_limite_inscripcion:
                msg = "La fecha de cierre no puede estar después de la fecha de inicio!"
                self.add_error('fecha_inicio', msg)
                self.add_error('fecha_limite_inscripcion', msg)

        if fecha_limite_inscripcion and fecha_fin:
            if fecha_fin < fecha_limite_inscripcion:
                msg = "La fecha de cierre no puede estar después de la fecha de fin!"
                self.add_error('fecha_limite_inscripcion', msg)
                self.add_error('fecha_fin', msg)

        if fecha_inicio and fecha_fin:
            if fecha_inicio > fecha_fin:
                msg = "La fecha de inicio no puede estar después de la fecha de fin!"
                self.add_error('fecha_inicio', msg)
                self.add_error('fecha_fin', msg)




    class Meta:
        model = Evento
        widgets = {
            'fecha_inicio': MyDateTimeWidget(),
            'fecha_limite_inscripcion': MyDateTimeWidget(),
            'fecha_fin': MyDateTimeWidget(),
        }
        #fields = ('nombre', 'descripcion', 'lugar', 'direccion', 'telefono', 'costo', 'numero_cupos')
        fields = ('nombre', 'descripcion', 'lugar', 'direccion', 'telefono', 'costo', 'numero_cupos', 'fecha_limite_inscripcion', 'fecha_inicio', 'fecha_fin', 'imagen')





class ModificarEventoForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ModificarEventoForm, self).__init__(*args, **kwargs)
        self.fields['nombre'].label = 'Nombre'
        self.fields['descripcion'].label = 'Descripción'
        self.fields['lugar'].label = 'Lugar'
        self.fields['direccion'].label = 'Dirección'
        self.fields['telefono'].label = 'Teléfono'
        self.fields['costo'].label = 'Costo'
        self.fields['estado'].label = 'Estado'
        self.fields['numero_cupos'].label = 'Número de cupos'
        self.fields['fecha_limite_inscripcion'].label = 'Fecha límite de inscripción'
        self.fields['fecha_inicio'].label = 'Fecha de inicio'
        self.fields['fecha_fin'].label = 'Fecha de fin'

        self.fields['nombre'].required = True
        self.fields['descripcion'].required = True
        self.fields['lugar'].required = True
        self.fields['direccion'].required = True
        self.fields['telefono'].required = True
        self.fields['costo'].required = True
        self.fields['estado'].required = True
        self.fields['numero_cupos'].required = True
        self.fields['fecha_limite_inscripcion'].required = True
        self.fields['fecha_inicio'].required = True
        self.fields['fecha_fin'].required = True

    class Meta:
        widgets = {
            'fecha_inicio': MyDateTimeWidget(),
            'fecha_limite_inscripcion': MyDateTimeWidget(),
            'fecha_fin': MyDateTimeWidget(),
        }

        model = Evento
        fields = ('nombre', 'descripcion', 'lugar', 'direccion', 'telefono', 'costo', 'numero_cupos','estado', 'fecha_limite_inscripcion', 'fecha_inicio', 'fecha_fin', 'imagen')

    def clean(self):
        cleaned_data = super().clean()
        fecha_inicio = cleaned_data.get('fecha_inicio')
        fecha_limite_inscripcion = cleaned_data.get('fecha_limite_inscripcion')
        fecha_fin = cleaned_data.get('fecha_fin')

        today_date = timezone.now()  # offset-awared datetime
        today_date.astimezone(timezone.utc).replace(tzinfo=None)

        if fecha_inicio:
            #print(today_date)
            #print(fecha_inicio)
            if fecha_inicio < today_date:
                msg = "La fecha de inicio no puede estar en el pasado!"
                self.add_error('fecha_inicio', msg)
        else:
            msg = "La fecha es requerida"
            self.add_error('fecha_inicio', msg)


        if fecha_limite_inscripcion:
            if fecha_limite_inscripcion < today_date:
                msg = "La fecha de cierre no puede estar en el pasado!"
                self.add_error('fecha_limite_inscripcion', msg)
        else:
            msg = "La fecha es requerida"
            self.add_error('fecha_inicio', msg)

        if fecha_fin:
            if fecha_fin < today_date:
                msg = "La fecha de fin no puede estar en el pasado!"
                self.add_error('fecha_fin', msg)
        else:
            msg = "La fecha es requerida"
            self.add_error('fecha_inicio', msg)

        if fecha_inicio and fecha_limite_inscripcion:
            if fecha_inicio < fecha_limite_inscripcion:
                msg = "La fecha de cierre no puede estar después de la fecha de inicio!"
                self.add_error('fecha_inicio', msg)
                self.add_error('fecha_limite_inscripcion', msg)

        if fecha_limite_inscripcion and fecha_fin:
            if fecha_fin < fecha_limite_inscripcion:
                msg = "La fecha de cierre no puede estar después de la fecha de fin!"
                self.add_error('fecha_limite_inscripcion', msg)
                self.add_error('fecha_fin', msg)

        if fecha_inicio and fecha_fin:
            if fecha_inicio > fecha_fin:
                msg = "La fecha de inicio no puede estar después de la fecha de fin!"
                self.add_error('fecha_inicio', msg)
                self.add_error('fecha_fin', msg)

class Pre_Inscribir(forms.ModelForm):

    class Meta():
        model = Participante
        fields = ('nombres', 'apellidos', 'correo', 'numero_documento', 'telefono')

class Inscribir(forms.Form):
    participante = forms.CharField(required=True, label='Número de documento')

    class Meta():
        fields = ('participante',)

class ModificarParticipanteForm(forms.ModelForm):


    class Meta():
        model = Participante
        fields = ('nombres', 'apellidos', 'correo', 'telefono')


class CodigoParticipanteForm(forms.Form):
    codigo =  forms.CharField(required = True, label = 'Ingrese su código para modificar datos personales', help_text="Su código tiene la forma ############-####")


class CorreoParticipanteForm(forms.Form):
    correo = forms.EmailField(required=True, label='Ingrese su correo electrónico para recuperar su código para modificación de datos',
                             help_text="Si existe un participante registrado con el correo ingresado le será enviado un mensaje con el código para modificación de datos")
