from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction, IntegrityError
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.urls.base import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, TemplateView
from proyectowww.utilidades import verificar_cargo
from .forms import *

from django.utils import timezone

from apps.actividad.models import Actividad
from .models import Evento
from apps.eventos.models import Evento, Inscripcion, Participante
from django.views.generic import FormView
from .forms import CrearEventoForm, Pre_Inscribir, Inscribir



@verificar_cargo(cargos_permitidos=["Operador", "Administrador"])
def crear_evento(request):
    if request.method == 'POST':
        form = CrearEventoForm(request.POST, request.FILES)

        if form.is_valid():
            form.save()
            messages.success(request, "El evento ha sido guardado correctamente")
            return redirect('evento_listar')
        messages.error(request, "Error al crear el evento")
    else:
        form = CrearEventoForm()
    return render(request, "crear_evento.html", {'form': form})


@verificar_cargo(cargos_permitidos=["Operador", "Administrador"])
def modificar_evento(request, id_evento):
    evento = Evento.revisar_existencia_evento(id_evento)
    # print(evento)
    if not evento:
        messages.success(request, "No existe el evento")
        return redirect('evento_listar')
    if request.method == 'POST':
        form = ModificarEventoForm(request.POST, request.FILES, instance=evento)
        if form.is_valid():
            form.save()
            messages.success(request, "El evento ha sido guardado correctamente")
            return redirect('evento_listar')
        messages.error(request, "Error al modificar el evento")
    else:
        form = ModificarEventoForm(instance=evento)
    return render(request, "modificar_evento.html", {'form': form})


@verificar_cargo(cargos_permitidos=["Operador", "Administrador"])
def activar_evento(request, id_evento):
    try:
        evento = Evento.objects.get(id=id_evento)
        evento.estado = 'Activo'
        evento.save()
        messages.success(request, "El evento ha sido activado correctamente")
    except Evento.DoesNotExist:
        messages.error(request, "El evento solicitado no existe")
    return redirect('evento_listar')


@verificar_cargo(cargos_permitidos=["Operador", "Administrador"])
def desactivar_evento(request, id_evento):
    try:
        evento = Evento.objects.get(id=id_evento)
        evento.estado = 'Inactivo'
        evento.save()
        messages.success(request, "El evento ha sido desactivado correctamente")
    except Evento.DoesNotExist:
        messages.error(request, "El evento solicitado no existe")
    return redirect('evento_listar')


class ListarEventos(ListView):
    model = Evento
    template_name = 'lista_eventos.html'
    queryset = Evento.objects.all()
    context_object_name = 'lista_eventos'

    def dispatch(self, request, *args, **kwargs):
        return super(ListarEventos, self).dispatch(request, *args, **kwargs)


def pre_inscripcion(request, evento_id):
    if request.method == 'POST':
        form = Pre_Inscribir(request.POST)
        id_participante = form.data['numero_documento']
        participante = Participante.revisar_existencia_participante(id_participante)
        if participante is None:
            if form.is_valid():
                try:
                    with transaction.atomic():
                        participante = form.save()
                        preinscripcion, mensaje = participante.pre_inscribir_en_evento(evento_id)
                except IntegrityError as e:
                    messages.error(request, "Error, fallo durante la preinscripción, por favor registre sus datos nuevamente")
                    return redirect(request.META.get('HTTP_REFERER','evento_listar'))


                if preinscripcion:
                    messages.success(request, mensaje)
                    # preinscripcion.enviar_correo_preinscripcion(request)
                    return redirect('detalles_evento', evento_id)
                else:
                    # participante.enviar_correo_preinscripcion_fallida(request, evento_id)
                    messages.error(request, mensaje)
            messages.error(request, "Error al registrar la preinscripción")
        else:
            messages.success(request, "La preinscripción se realizó con sus datos existente en el sistema")
            evento = Evento.revisar_existencia_evento(evento_id)
            if Inscripcion.revisar_participacion(participante, evento):
                messages.error(request, "El participante ya está registrado en el vento")
                return redirect('detalles_evento', evento_id)
            preinscripcion, mensaje = participante.pre_inscribir_en_evento(evento_id)
            if preinscripcion:
                messages.success(request, mensaje)
                # preinscripcion.enviar_correo_preinscripcion(request)
                return redirect('detalles_evento', evento_id)
            else:
                # participante.enviar_correo_preinscripcion_fallida(request, evento_id)
                messages.error(request, mensaje)


    else:
        form = Pre_Inscribir()
    contexto = {'form': form}
    return render(request, 'pre_inscripcion.html', contexto)


def inscribir_en_evento(request, evento_id):
    participante = None
    if request.method == 'POST':

        form = Inscribir(request.POST)
        participante = form.data['participante']
        if form.is_valid():
            participante = Participante.revisar_existencia_participante(participante)

            evento = Evento.revisar_existencia_evento(evento_id)
            fecha_inicio = evento.fecha_inicio;
            fecha_fin = evento.fecha_fin;
            today_date = timezone.now()  # offset-awared datetime
            today_date.astimezone(timezone.utc).replace(tzinfo=None)

            #if True:
            if fecha_inicio < today_date and today_date < fecha_fin:
                if participante:
                    id_participante = participante.id
                    # print(id_participante)
                    # print(evento_id)
                    participacion = Inscripcion.get_participacion(id_participante, evento_id)

                    if participacion:
                        participacion.confirmar_pago_web_service(request)

                        if participacion.estado_inscripcion == 'INSCRITO':
                            messages.error(request, "El participante ya está inscrito")
                            return redirect('inscribir_en_evento', evento_id)
                        elif participacion.estado_inscripcion == 'PAGO':

                            inscripcion = participacion.inscribir()

                            if inscripcion:
                                messages.success(request, "Usuario inscrito correctamente")

                                # crear_escarapela(request, evento_id, id_participante)
                                return redirect('crear_escarapela', evento_id, id_participante)
                                # $('#id_enlace').on(click, funcion).trigger()
                                # return redirect('inscribir_en_evento', evento_id)
                            else:
                                messages.error(request, "Error al registrar la inscripción")
                        else:
                            messages.error(request, "Error al registrar la inscripción")
                    else:
                        messages.error(request, "Error al registrar la inscripción: el participante no esta inscrito")

                else:
                    messages.error(request, "Error al registrar la inscripción: el participante no existe")
            else:
                messages.error(request,
                               "Error al registrar la inscripción: El evento no ha iniciado o ya llego a su fin")
    else:
        form = Inscribir()

    contexto = {'form': form}
    return render(request, 'inscripcion.html', contexto)


def detalles_evento(request, evento_id):
    evento = Evento.revisar_existencia_evento(evento_id)
    if (evento is not False):
        nombre_evento = evento.nombre
        descripcion_evento = evento.descripcion
        lugar_evento = evento.lugar
        direccion_evento = evento.direccion
        telefono_evento = evento.telefono
        costo_evento = evento.costo
        numero_cupos_evento = evento.numero_cupos
        fecha_limite_inscripcion_evento = evento.fecha_limite_inscripcion
        fecha_inicio_evento = evento.fecha_inicio
        fecha_fin_evento = evento.fecha_fin
        imagen = evento.imagen

        lista_actividades = obtener_lista_actividades(evento)
        lista_participantes = obtener_lista_participantes(evento)
        print (evento.imagen)
        contexto = {
            'id': evento_id,
            'nombre': nombre_evento,
            'descripcion': descripcion_evento,
            'lugar': lugar_evento,
            'direccion': direccion_evento,
            'telefono': telefono_evento,
            'costo': costo_evento,
            'numero_cupos': numero_cupos_evento,
            'fecha_limite': fecha_limite_inscripcion_evento,
            'fecha_inicio': fecha_inicio_evento,
            'fecha_fin': fecha_fin_evento,
            'lista_actividades': lista_actividades,
            'lista_participantes': lista_participantes,
            'imagen': imagen,
        }

        return render(request, 'detalles_evento.html', contexto)
    else:
        return redirect('evento_listar')


def obtener_lista_actividades(_evento):
    lista_actividades = Actividad.objects.filter(evento=_evento)
    return lista_actividades


def obtener_lista_participantes(_evento):
    # lista_inscripciones = Inscripcion.objects.filter(evento=_evento)
    evento = Evento.objects.get(id=_evento.id)
    lista_participantes = evento.participantes.all()
    return lista_participantes


def listar_participante(request):
    lista_participante = Participante.objects.all()
    return render(request, "listado_participante.html", {"listar_participante": lista_participante, })


@verificar_cargo(cargos_permitidos=["Operador"])
def listar_preinscrito(request, evento_id):
    try:
        evento = Evento.objects.get(id=evento_id)
    except ObjectDoesNotExist:
        messages.error(request, "No existe el evento")
        return redirect('evento_listar')

    return render(request, "listado_preinscrito.html", {"evento": evento, })


@verificar_cargo(cargos_permitidos=["Operador"])
def rechazar_preinscrito(request, evento_id, inscripcion_id):
    try:
        evento = Evento.objects.get(id=evento_id)
        preinscrito = Inscripcion.objects.get(id=inscripcion_id)
        if preinscrito.estado_inscripcion == "EN ESPERA PRE":
            preinscrito.estado_inscripcion = 'Rechazado'
            preinscrito.save()
            preinscrito.enviar_correo_preinscripcion_rechazada(request)
            messages.success(request, "El participante ha sido rechazado correctamente")
    except Evento.DoesNotExist:
        messages.error(request, "No existe el evento")
        return redirect('evento_listar')

    except ObjectDoesNotExist:
        messages.error(request, "No es posible rechazar la inscripción")

    return redirect('listar_preinscrito', evento_id)


@verificar_cargo(cargos_permitidos=["Operador"])
def aceptar_preinscrito(request, evento_id, inscripcion_id):
    try:
        evento = Evento.objects.get(id=evento_id)
        preinscrito = Inscripcion.objects.get(id=inscripcion_id)
        if preinscrito.estado_inscripcion == "EN ESPERA PRE":
            preinscrito.estado_inscripcion = 'PREINSCRITO'
            preinscrito.save()
            preinscrito.enviar_correo_preinscripcion_aceptada(request)
            messages.success(request, "El participante ha sido aceptado correctamente")
    except Evento.DoesNotExist:
        messages.error(request, "No existe el evento")
        return redirect('evento_listar')


    except ObjectDoesNotExist:
        messages.error(request, "No es posible aceptar la inscripción")

    return redirect('listar_preinscrito', evento_id)


@verificar_cargo(cargos_permitidos=["Operador"])
def participante_modificar(request, evento_id, participante_id):
    participante = Participante.revisar_existencia_participante_id(participante_id)
    # print(evento)
    if not participante:
        messages.success(request, "No existe el participante")
        return redirect('listar_preinscrito', evento_id)
    if request.method == 'POST':
        form = ModificarParticipanteForm(request.POST, request.FILES, instance=participante)
        if form.is_valid():
            form.save()
            messages.success(request, "El participante ha sido guardado correctamente")
            return redirect('listar_preinscrito', evento_id)
        messages.error(request, "Error al modificar el participante")
    else:
        form = ModificarParticipanteForm(instance=participante)
    return render(request, "modificar_participante.html", {'form': form})


def crear_escarapela(request, evento_id, participante_id):
    participante = Participante.revisar_existencia_participante_id(participante_id)
    evento = Evento.revisar_existencia_evento(evento_id)
    inscripcion = Inscripcion.revisar_participacion(participante, evento)

    if (inscripcion is not False):

        contexto = {
            'participante': participante,
            'evento': evento,
        }
        messages.success(request, "Escarapela creada correctamente")
        return render(request, 'crear_escarapela.html', contexto)
    else:
        messages.error(request, "Error al crear la escarapela")
        return redirect('crear_escarapela', evento_id, participante_id)


def CalendarioEvento(request, evento_id):
    evento = Evento.revisar_existencia_evento(evento_id)
    # print(evento)
    if not evento:
        messages.success(request, "No existe el evento")
        return redirect('evento_listar')

    import random
    color_aleatorio = lambda: random.randint(0, 255)

    datos = []

    for actividad in evento.actividades.all():
        datos.append({
            "fechaInicio": '\'%s %s\'' % (actividad.fecha_realizacion, actividad.hora_inicio),
            "fechaFin": '"%s %s"' % (actividad.fecha_realizacion, actividad.hora_fin),
            "titulo": actividad.nombre,
            "color": '#%02X%02X%02X' % (color_aleatorio(), color_aleatorio(), color_aleatorio())
        })

    return render(request, "calendario_evento.html", {'datos': datos, 'evento': evento})




def datos_calendario(request):
    import random

    color_aleatorio = lambda: random.randint(0, 255)

    evento = Evento.objects.get(pk = request.GET['pk_evento'])

    lista = []


    for actividad in evento.actividades.all():
        dato = {
            'titulo': actividad.nombre,
            'fechaInicio': "%s %s" %(actividad.fecha_realizacion,actividad.hora_inicio),
            'FechaFin': "%s %s" %(actividad.fecha_realizacion,actividad.hora_fin),
            'color': '#%02X%02X%02X' % (color_aleatorio(), color_aleatorio(), color_aleatorio())
        }
        lista.append(dato)
    datos_enviar = {
        'datos': lista
    }
    return JsonResponse(datos_enviar)


def participante_modificar_datos_personales(request):
    participante = Participante.revisar_existencia_participante_id(request.session.get('id_participante',None))

    if not participante:
        messages.success(request, "No existe el participante")
        return redirect('evento_listar')
    if request.method == 'POST':
        form = Pre_Inscribir(request.POST, instance=participante)
        if form.is_valid():
            form.save()
            try:
                del request.session['id_participante']
            except Exception:
                pass

            messages.success(request, "Los datos han sido actualizados correctamente")
            return redirect('inicio')
        messages.error(request, "Error al modificar los datos")
    else:
        form = Pre_Inscribir(instance=participante)
    return render(request, "modificar_datos_personales_participante.html", {'form': form})

def participante_ingresar_codigo(request):
    if request.method == 'POST':
        form = CodigoParticipanteForm(request.POST)
        if form.is_valid():
            participante = Participante.buscar_participante_codigo(form.cleaned_data['codigo'])
            if not participante:
                messages.error(request, "No existe el participante")
                return redirect(request.META.get('HTTP_REFERER','evento_listar'))

            request.session['id_participante'] = participante.id
            return redirect('participante_modificar')
        messages.error(request, "Por favor verifique los campos")
    else:
        form = CodigoParticipanteForm()
    return render(request, "modificar_datos_codigo_participante.html", {'form': form})


def recuperar_codigo(request):
    if request.method == 'POST':
        form = CorreoParticipanteForm(request.POST)
        if form.is_valid():
            participante = Participante.revisar_existencia_participante_correo(form.cleaned_data['correo'])
            if not participante:
                messages.error(request, "No existe el participante")
                return redirect(request.META.get('HTTP_REFERER','evento_listar'))
            participante.enviar_correo_recuperacion_codigo()

            messages.info('En caso de que el correo ingresado se encuentre registrado se enviará la información para recuperar el código de acceso')

            return redirect(request.META.get('HTTP_REFERER', 'evento_listar'))
        messages.error(request, "Por favor verifique los campos")
    else:
        form = CorreoParticipanteForm()
    return render(request, "recuperar_codigo_correo.html", {'form': form})

