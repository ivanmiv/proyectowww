from django.shortcuts import render, redirect
from apps.noticias.models import Noticia
from datetime import date, timedelta, datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from apps.usuario.models import Usuario
from apps.eventos.models import Evento
from apps.actividad.models import Actividad
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages
# Create your views here.

def Index(request):
    #Usuario.crear_usuario_inicial()
    if (not request.user.is_anonymous) and request.user.cargo == "Administrador":
        return redirect('usuario_listar')

    try:
        lista_noticias_slider = Noticia.objects.all()[:5]
        lista_noticias = Noticia.objects.all()[5:]

        paginator = Paginator(lista_noticias, 5) # Show 5 per page
        page = request.GET.get('page')
        try:
            noticias = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            noticias = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            noticias = paginator.page(paginator.num_pages)

        posiciones = obtener_lista_paginas(noticias)

        contexto = {

            'lista_noticias' : noticias, 
            'paginas' : posiciones,
            'noticias' : lista_noticias_slider}

        #return render(request, "slider2.html", None)
        return render(request,"slider_noticias.html", contexto)
    except Noticia.DoesNotExist:
        messages.error(request, "Error")
        return redirect('inicio')

def ver_eventos(request):
    if (not request.user.is_anonymous) and request.user.cargo == "Administrador":
        return redirect('usuario_listar')

    try:
        lista_eventos_slider = Evento.objects.all()[:5]
        lista_eventos = Evento.objects.all()[5:]

        paginator = Paginator(lista_eventos, 5) # Show 5 per page
        page = request.GET.get('page')
        try:
            eventos = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            eventos = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            eventos = paginator.page(paginator.num_pages)

        posiciones = obtener_lista_paginas(eventos)

        contexto = {

            'lista_eventos' : eventos, 
            'paginas' : posiciones,
            'eventos' : lista_eventos_slider}

        #return render(request, "slider2.html", None)
        return render(request,"slider_eventos.html", contexto)
    except Evento.DoesNotExist:
        messages.error(request, "Error")
        return redirect('inicio')
    
def obtener_lista_paginas(paginador):

    actual = paginador.number
    total = paginador.paginator.num_pages

    lista_posiciones = [0]*5

    lista_posiciones[2] = actual

    if(total < 6):
        return range(1, total + 1)

    if actual < 3:
        lista_posiciones = range(1, 6)

    elif( actual + 2 > total ):
        index = 0
        for i in range(total - 4, total +1):
            
            lista_posiciones[index] = i
            index+=1

    elif(actual >= 3):
        for i in range(0, len(lista_posiciones)):
            if (i < 2):
                lista_posiciones[i] = lista_posiciones[2] - (2 - i)
            elif(i > 2):
                lista_posiciones[i] = lista_posiciones[2] + (i - 2)

    return lista_posiciones

def ver_noticia(request, id_noticia):
    try:
        noticia = Noticia.objects.get(id=id_noticia)
        if (noticia is not False):

            contexto = {
                'noticia': noticia,
            }

            return render(request, 'detalle_noticia.html', contexto)
        else:
            return redirect('evento_listar')
    except Noticia.DoesNotExist:
        messages.error(request, "La noticia no existe")
        return redirect('inicio')

def ver_evento(request, id_evento):
    try:
        _evento = Evento.objects.get(id=id_evento)
        lista_actividades = Actividad.objects.filter(evento = _evento)
        if (_evento is not False):

            contexto = {
                'evento': _evento,
                'lista_actividades' : lista_actividades
            }

            return render(request, 'detalle_evento.html', contexto)
        else:
            return redirect('evento_listar')
    except Evento.DoesNotExist:
        messages.error(request, "El evento no existe")
        return redirect('inicio')

def nosotros(request):
    return render(request, "nosotros.html", None)