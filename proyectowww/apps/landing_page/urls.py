from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.Index, name = 'inicio'),
    url(r'^ver_noticia/(?P<id_noticia>\d+)$', views.ver_noticia, name='ver_noticia'),
    url(r'^ver_evento/(?P<id_evento>\d+)$', views.ver_evento, name='ver_evento'),
    url(r'^eventos$', views.ver_eventos, name='eventos'),
    url(r'^nosotros$', views.nosotros, name='nosotros'),
]