from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from django.views.generic import ListView
from proyectowww.utilidades import verificar_cargo
from .forms import CrearNoticiaForm
from apps.eventos.models import Evento
from .models import *
# Create your views here.
'''
def inicio(request):
    return HttpResponse("Not finished yet")
'''


@verificar_cargo(cargos_permitidos=['Operador','Gerente'])
def crear_modificar_noticia(request, id_noticia = None, id_evento = None):
    noticia = Noticia.obtener_noticia(id_noticia)
    evento = Evento.revisar_existencia_evento(id_evento)

    if id_noticia and not noticia:
        messages.error(request, "No existe la noticia")
        return redirect('noticia_listar')

    if id_evento and not evento:
        messages.error(request, "No existe el evento que intenta asociar a la noticia")
        return redirect('noticia_listar')

    if request.method == 'POST':
        form = CrearNoticiaForm(request.POST, request.FILES,instance = noticia, evento_elegido= evento)

        if form.is_valid():
            if  noticia:
                form.save()
                messages.success(request, "La noticia ha sido actualizada correctamente")
            else:
                noticia = form.save(commit=False)
                noticia.creador=request.user
                noticia.save()
                messages.success(request, "La noticia ha sido registrada correctamente")
            return redirect('noticia_listar')

        messages.error(request, "Error al crear la noticia")

    else:
        form = CrearNoticiaForm(instance=noticia, evento_elegido= evento)

    contexto = {'form': form, 'noticia':noticia, 'evento':evento}
    return render(request, 'crear_noticia.html', contexto)


@verificar_cargo(cargos_permitidos=["Operador"])
def noticias_listar(request):
    lista_noticias = Noticia.objects.all()

    contexto = {'lista_noticias': lista_noticias}
    return render(request, 'listar_noticias.html', contexto)

@verificar_cargo(cargos_permitidos=["Operador"])
def activar_noticia(request, id_noticia):
    try:
        noticia = Noticia.objects.get(id=id_noticia)
        noticia.estado = 'ACTIVO'
        noticia.save()
        messages.success(request, "La noticia ha sido activada correctamente")
    except Noticia.DoesNotExist:
        messages.error(request, "La noticia solicitada no existe")
    return redirect('noticia_listar')


@verificar_cargo(cargos_permitidos=["Operador"])
def desactivar_noticia(request, id_noticia):
    try:
        noticia = Noticia.objects.get(id=id_noticia)
        noticia.estado = 'INACTIVO'
        noticia.save()
        messages.success(request, "La noticia ha sido desactivada correctamente")
    except Noticia.DoesNotExist:
        messages.error(request, "La noticia solicitada no existe")
    return redirect('noticia_listar')


def detalles_noticia(request, id_noticia):
    noticia = Noticia.objects.get(id=id_noticia)
    titulo = noticia.titulo
    tag = noticia.tag
    descripcion = noticia.descripcion
    fecha_publicacion = noticia.fecha_publicacion
    creador = noticia.creador
    imagen = noticia.imagen

    contexto = {
        'id': id_noticia,
        'titulo': titulo,
        'descripcion': descripcion,
        'tag': tag,
        'fecha_publicacion': fecha_publicacion,
        'creador': creador,
        'imagen': imagen,
        }

    return render(request, 'detalles_noticia.html', contexto)