from django import forms
from .models import Noticia

class CrearNoticiaForm(forms.ModelForm):
    """docstring for CrearNoticiaForm"""
    #nombre = forms.CharField(required = True)
    #descripcion = forms.CharField(required = True, widget = forms.Textarea)

    def __init__(self, *args, **kwargs):
        self.evento_elegido = kwargs.pop('evento_elegido', None)
        super(CrearNoticiaForm, self).__init__(*args, **kwargs)

        if self.evento_elegido:
            self.fields['evento'].initial = self.evento_elegido
            self.fields['evento'].widget = forms.HiddenInput()
        else:
            self.fields['evento'].initial = None
            self.fields['evento'].widget = forms.HiddenInput()

    class Meta():
        model = Noticia
        fields = ('titulo', 'tag' , 'descripcion', 'imagen', 'evento')

    def clean_titulo(self):
        titulo = self.cleaned_data['titulo']
        if titulo is None or titulo=="":
            self._errors['titulo']= ['Complete este campo']

        return  titulo
