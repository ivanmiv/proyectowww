from django.conf.urls import url
from . import views

urlpatterns = [
    #url(r'^$', views.inicio, name='inicio'),
    url(r'^crear$', views.crear_modificar_noticia, name ='noticia_crear'),
    url(r'^crear/(?P<id_evento>\d+)$', views.crear_modificar_noticia, name ='noticia_crear'),
    url(r'^modificar/(?P<id_noticia>\d+)$', views.crear_modificar_noticia, name ='noticia_modificar'),
    url(r'listar$', views.noticias_listar, name = 'noticia_listar'),
    url(r'^activar/(?P<id_noticia>\d+)$', views.activar_noticia, name ='activar_noticia'),
    url(r'^desactivar/(?P<id_noticia>\d+)$', views.desactivar_noticia, name ='desactivar_noticia'),
    url(r'^ver/(?P<id_noticia>\d+)/$', views.detalles_noticia, name = 'detalles_noticia'),
]
