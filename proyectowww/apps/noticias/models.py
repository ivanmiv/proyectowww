from django.db import models


# from apps.eventos import models

def crear_ruta_foto_noticia(instance, filename):
    return "fotos-noticias/%s-%s" % (instance.id, filename.encode('ascii', 'ignore'))


class Noticia(models.Model):
    ESTADO = (
        ('ACTIVO', 'ACTIVO'),
        ('INACTIVO', 'INACTIVO')
    )

    titulo = models.CharField(max_length=250, unique=True, verbose_name='título')
    tag = models.CharField(max_length=150,verbose_name='etiqueta')
    descripcion = models.TextField(verbose_name='descripción', max_length=1200)
    estado = models.CharField(max_length=20, choices=ESTADO, default='ACTIVO')
    fecha_publicacion = models.DateTimeField(auto_now_add=True)
    evento = models.ForeignKey('eventos.Evento', blank=True, null=True)  # null y blank en true para que sea opcional
    imagen = models.ImageField(upload_to=crear_ruta_foto_noticia, help_text='Seleccione una imagen para la noticia')
    creador = models.ForeignKey('usuario.Usuario')


    class Meta:
        ordering = ["-fecha_publicacion"]

    @staticmethod
    def obtener_noticia(id_noticia):
        try:
            return Noticia.objects.get(id=id_noticia)
        except Exception:
            return None

    def is_active(self):
        if(self.estado == 'ACTIVO'):
            return True
        else:
            return False
