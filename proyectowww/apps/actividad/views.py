from django.shortcuts import render, redirect
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.contrib import messages
from django.urls.base import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import *
from .models import Actividad
from .forms import *

from proyectowww.utilidades import verificar_cargo
from apps.actividad.models import Actividad
from apps.eventos.models import Evento, Participante, Inscripcion
from datetime import *
from time import *


@verificar_cargo(cargos_permitidos=["Operador"])
def modificar_actividad(request, id_actividad):
    actividad = Actividad.revisar_existencia_actividad(id_actividad)

    if not actividad:
        messages.error(request, "Error al modificar la actividad")
        return redirect("actividad_listar")

    if request.method == 'POST':
        form = ModificarActividadForm(request.POST, request.FILES, instance=actividad)
        if form.is_valid():
            form.save()
            messages.success(request, "La actividad ha sido guardada correctamente")
            return redirect('actividad_listar')
        messages.error(request, "Error al modificar la actividad")
    else:
        form = ModificarActividadForm(instance=actividad)

    return render(request, "actividad_modificar.html", {'form': form})


@verificar_cargo(cargos_permitidos=["Gerente", "Operador"])
def listado_actividades(request):
    listado_actividades = Actividad.objects.all()
    return render(request, "actividad_listado.html", {
        "listado_act": listado_actividades,
    })


@verificar_cargo(cargos_permitidos=["Operador"])
def crear_actividad(request):
    if request.method == "POST":
        form = RegistroActividad(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, "La actividad ha sido registrada correctamente")
            return redirect("actividad_listar")
    else:
        form = RegistroActividad()
    return render(request, "registro_actividad.html", {
        "form": form,
    })


@verificar_cargo(cargos_permitidos=["Operador"])
def crear_actividad_evento(request, id_evento):
    evento = Evento.revisar_existencia_evento(id_evento)

    if evento ==  False:
        messages.error(request, "El evento asociado no existe")
        return redirect("evento_listar")

    if evento.estado != 'Activo':
        messages.error(request, "El evento asociado no está activo")
        return redirect("evento_listar") 


    if request.method == "POST":

        form = RegistroActividadEvento(request.POST, request.FILES, evento_elegido = evento)
        if form.is_valid():
            form.save()
            messages.success(request, "La actividad ha sido registrada correctamente")
            return redirect("actividad_listar")
    else:
        form = RegistroActividadEvento( evento_elegido = evento)
    return render(request, "registro_actividad_evento.html", {
        "form": form,
    })


@verificar_cargo(cargos_permitidos=["Operador"])
def activar_actividad(request, id_actividad):
    try:
        actividad = Actividad.objects.get(id=id_actividad)
        actividad.estado = 'Activo'
        actividad.save()
        messages.success(request, "La actividad ha sido activada correctamente")
    except Actividad.DoesNotExist:
        messages.error(request, "La actividad solicitada no existe")
    return redirect('actividad_listar')


@verificar_cargo(cargos_permitidos=["Operador"])
def desactivar_actividad(request, id_actividad):
    try:
        actividad = Actividad.objects.get(id=id_actividad)
        actividad.estado = 'Inactivo'
        actividad.save()
        messages.success(request, "La actividad ha sido desactivada correctamente")
    except Actividad.DoesNotExist:
        messages.error(request, "La actividad solicitada no existe")
    return redirect('actividad_listar')


@verificar_cargo(cargos_permitidos=["Operador"])
def listado_participantes(request, id_actividad):
    try:
        actividad = Actividad.objects.get(id=id_actividad)
        print(actividad)
        _evento = actividad.evento

        _time = datetime.now()
        hora_actual = datetime.time(_time)

        if (actividad.fecha_realizacion <= date.today() and actividad.hora_fin <= hora_actual):
            messages.error(request, "La actividad ya ha terminado")
            return redirect('actividad_listar')

        if (actividad.fecha_realizacion != date.today()):
            messages.error(request, "La actividad no es el día de hoy. Por favor espere al día de la actividad para inscribir participantes")
            return redirect('actividad_listar')

        participantes_evento = Inscripcion.objects.filter(evento=_evento, estado_inscripcion='INSCRITO')

        participantes_en_actividad = actividad.participantes
        lista_participantes = []

        for p in participantes_evento:
            if (p.participante not in participantes_en_actividad.all()):
                lista_participantes.append(p.participante)

        contexto = {
            'evento': _evento,
            'lista_participantes': lista_participantes,
            'actividad': actividad,
        }

        return render(request, 'inscripcion_actividad.html', contexto)
    except Actividad.DoesNotExist:
        messages.error(request, "La actividad no existe")
        return redirect('actividad_listar')

@verificar_cargo(cargos_permitidos=['Operador'])
def inscribir_participante_actividad(request, numero_identificacion, id_actividad):
    try:
        actividad = Actividad.objects.get(id=id_actividad)

        participante = Participante.objects.get(numero_documento=numero_identificacion)

        participantes_en_actividad = actividad.participantes
        if (participante in participantes_en_actividad.all()):
            messages.error(request, "El participante ya está registrado en la actividad")
            return redirect('listado_participantes', id_actividad)

        actividad.participantes.add(participante)
        messages.success(request, "El participante ha sido registrado en la actividad")


    except Participante.DoesNotExist:
        messages.error(request, "El participante no está registrado")
        return redirect('actividad_listar')
    except Actividad.DoesNotExist:
        messages.error(request, "La actividad no existe")
        return redirect('actividad_listar')

    return redirect('actividad_listar')

@verificar_cargo(cargos_permitidos=['Operador'])
def registrar_resultado(request, id_actividad):
    try:
        actividad = Actividad.objects.get(id=id_actividad)

    
        if(actividad is None):
            messages.error(request, "La actividad no existe")
            return redirect('actividad_listar')

        _time = datetime.now()
        hora_actual = datetime.time(_time)
        _day = datetime.now()
        fecha_actual = datetime.date(_day)


        if(actividad.hora_fin > hora_actual and actividad.fecha_realizacion > fecha_actual):
            messages.error(request, "La actividad no ha terminado. Por favor espere a que esta termine para registrar un resultado")
            return redirect('actividad_listar')

        resultado = Resultado.objects.filter(actividad=actividad)

        if(resultado):
            messages.info(request, "La actividad ya tiene un resultado")
            return redirect('modificar_resultado', id_actividad)

        if request.method == "POST":
            form = RegistroResultado(request.POST, actividad_elegida = actividad)
            if form.is_valid():
                form.save()
                messages.success(request, "El resultado de la actividad ha sido registrado correctamente")
                return redirect("actividad_listar")
        else:
            form = RegistroResultado(actividad_elegida = actividad)

        contexto = {
            'form' : form,
            'actividad': actividad,

        }
        return render(request, 'registro_resultado.html', contexto)
    except Actividad.DoesNotExist:
        messages.error(request, "La actividad no existe")
        return redirect('actividad_listar')

@verificar_cargo(cargos_permitidos=['Operador'])
def modificar_resultado(request, id_actividad):
    try:
        actividad = Actividad.objects.get(id=id_actividad)
        resultado = Resultado.objects.filter(actividad=actividad)[0]

        if(actividad is None):
            messages.error(request, "La actividad no existe")
            return redirect('actividad_listar')

        if(resultado is None):
            messages.error(request, "El resultado no existe")
            return redirect('actividad_listar')

        _time = datetime.now()
        hora_actual = datetime.time(_time)
        _day = datetime.now()
        fecha_actual = datetime.date(_day)


        if(actividad.hora_fin > hora_actual and actividad.fecha_realizacion > fecha_actual):
            messages.error(request, "La actividad no ha terminado. Por favor espere a que esta termine para registrar un resultado")
            return redirect('actividad_listar')


        if request.method == 'POST':
            form = ModificacionResultado(request.POST, instance = resultado)
            if form.is_valid():
                form.save()
                messages.success(request, "El resultado de la actividad ha sido modificado correctamente")
                return redirect("actividad_listar")
        else:
            form = ModificacionResultado(instance = resultado)

        contexto = {
            'form' : form,
            'actividad': actividad,

        }
        return render(request, 'registro_resultado.html', contexto)
    except Actividad.DoesNotExist:
        messages.error(request, "La actividad no existe")
        return redirect('actividad_listar')
    except Resultado.DoesNotExist:
        messages.error(request, "El resultado no existe")
        return redirect('actividad_listar')