from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^crear$', crear_actividad, name ="actividad_crear"),
    url(r'^crear2/(?P<id_evento>\d+)$', crear_actividad_evento, name ="actividad_crear_evento"),
    url(r'^modificar/(?P<id_actividad>\d+)$', modificar_actividad, name ='actividad_modificar'),
    url(r'listar$', listado_actividades, name = 'actividad_listar'),

    url(r'desactivar/(?P<id_actividad>\d+)$', desactivar_actividad, name='actividad_desactivar'),
    url(r'activar/(?P<id_actividad>\d+)$', activar_actividad, name = 'actividad_activar'),
    
    url(r'(?P<id_actividad>\d+)/listado_participantes$', listado_participantes, name='listado_participantes'),
    url(r'inscribir/(?P<numero_identificacion>[0-9]+)/(?P<id_actividad>[0-9]+)/', inscribir_participante_actividad, name = 'inscribir_participante_actividad'),
    url(r'(?P<id_actividad>\d+)/registrar_resultado', registrar_resultado, name='registrar_resultado'),
    url(r'(?P<id_actividad>\d+)/modificar_resultado', modificar_resultado, name='modificar_resultado'),
]