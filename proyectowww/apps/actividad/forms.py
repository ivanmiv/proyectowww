from django import forms
from .models import Actividad, Evento, Resultado
from proyectowww.utilidades import MyDateWidget, MyTimeWidget
from django.utils import timezone
import datetime

class RegistroActividad(forms.ModelForm):
    class Meta:
        model = Actividad
        widgets = {
            'fecha_realizacion': MyDateWidget(),
            'hora_inicio': MyTimeWidget(),
            'hora_fin': MyTimeWidget(),
        }
        exclude = ('participantes',)

    def clean(self):
        cleaned_data = super().clean()
        fecha_realizacion = cleaned_data.get('fecha_realizacion')
        hora_inicio = self.cleaned_data.get('hora_inicio')
        hora_fin = self.cleaned_data.get('hora_fin')
        today_date = datetime.datetime.now().strftime("%Y%m%d")
        cupo_disponible = self.cleaned_data['cupo_disponible']

        if fecha_realizacion:
            fecha_realizacion = fecha_realizacion.strftime("%Y%m%d")
            if fecha_realizacion < today_date:
                msg = "La fecha de realización de la actividad no debe ser pasada!"
                self.add_error('fecha_realizacion', msg)


        if hora_inicio and hora_fin and hora_fin < hora_inicio:
            msg = "La hora de inicio debe ser inferior a la hora de finalización"
            self.add_error('hora_inicio', msg)

        evento = self.cleaned_data['evento']
        if cupo_disponible <=0:
            msg = "El número de cupos de la actividad no puede ser cero!"
            self.add_error('cupo_disponible', msg)

        if cupo_disponible > evento.numero_cupos:
            msg = "El número de cupos de la actividad no puede ser superior al número de cupos del evento!"
            self.add_error('cupo_disponible', msg)

        if fecha_realizacion < evento.fecha_inicio.strftime("%Y%m%d"):
            msg = "La fecha de la actividad debe estar dentro de las fechas del evento asociado"
            self.add_error('fecha_realizacion', msg)

        if fecha_realizacion > evento.fecha_fin.strftime("%Y%m%d"):
            msg = "La fecha de la actividad debe estar dentro de las fechas del evento asociado"
            self.add_error('fecha_realizacion', msg)


class RegistroActividadEvento(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.evento_elegido = kwargs.pop('evento_elegido', None)
        super(RegistroActividadEvento, self).__init__(*args, **kwargs)

        self.fields['evento'].initial = self.evento_elegido
        self.fields['evento'].widget = forms.HiddenInput()

    class Meta:
        model = Actividad
        widgets = {
            'fecha_realizacion': MyDateWidget(),
            'hora_inicio': MyTimeWidget(),
            'hora_fin': MyTimeWidget(),
        }
        exclude = ('participantes',)

    def clean(self):
        cleaned_data = super().clean()
        fecha_realizacion = cleaned_data.get('fecha_realizacion')
        hora_inicio = self.cleaned_data.get('hora_inicio')
        hora_fin = self.cleaned_data.get('hora_fin')
        today_date = datetime.datetime.now().strftime("%Y%m%d")
        cupo_disponible = self.cleaned_data['cupo_disponible']

        if fecha_realizacion:
            fecha_realizacion = fecha_realizacion.strftime("%Y%m%d")
            if fecha_realizacion < today_date:
                msg = "La fecha de realización de la actividad no debe ser pasada!"
                self.add_error('fecha_realizacion', msg)


        if hora_inicio and hora_fin and hora_fin < hora_inicio:
            msg = "La hora de inicio debe ser inferior a la hora de finalización"
            self.add_error('hora_inicio', msg)

        evento = self.cleaned_data['evento']
        if cupo_disponible <=0:
            msg = "El número de cupos de la actividad no puede ser cero!"
            self.add_error('cupo_disponible', msg)

        if cupo_disponible > evento.numero_cupos:
            msg = "El número de cupos de la actividad no puede ser superior al número de cupos del evento!"
            self.add_error('cupo_disponible', msg)

        if fecha_realizacion < evento.fecha_inicio.strftime("%Y%m%d"):
            msg = "La fecha de la actividad debe estar dentro de las fechas del evento asociado"
            self.add_error('fecha_realizacion', msg)

        if fecha_realizacion > evento.fecha_fin.strftime("%Y%m%d"):
            msg = "La fecha de la actividad debe estar dentro de las fechas del evento asociado"
            self.add_error('fecha_realizacion', msg)


class ModificarActividadForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ModificarActividadForm, self).__init__(*args, **kwargs)
        self.fields['nombre'].label = 'Nombre'
        self.fields['descripcion'].label = 'Descripción'
        self.fields['lugar'].label = 'Lugar'
        self.fields['estado'].label = 'Estado'
        self.fields['fecha_realizacion'].label = 'Fecha de realización'
        self.fields['hora_inicio'].label = 'Hora de inicio'
        self.fields['hora_fin'].label = 'Hora de finalización'
        self.fields['cupo_disponible'].label = 'Cupo total'
        self.fields['responsable_actividad'].label = 'Responsable'
        #self.fields['numero_participantes'].label = 'Número de participantes'

        self.fields['nombre'].required = True
        self.fields['descripcion'].required = True
        self.fields['estado'].required = True
        self.fields['responsable_actividad'].required = False
        self.fields['cupo_disponible'].required = True

    

    class Meta:
        model = Actividad
        widgets = {
            'fecha_realizacion': MyDateWidget(),
            'hora_inicio': MyTimeWidget(),
            'hora_fin': MyTimeWidget(),
        }
        exclude = ('participantes',)

    def clean(self):
        cleaned_data = super().clean()
        fecha_realizacion = cleaned_data.get('fecha_realizacion')
        hora_inicio = self.cleaned_data.get('hora_inicio')
        hora_fin = self.cleaned_data.get('hora_fin')
        today_date = datetime.datetime.now().strftime("%Y%m%d")
        cupo_disponible = self.cleaned_data['cupo_disponible']


        if fecha_realizacion:
            fecha_realizacion = fecha_realizacion.strftime("%Y%m%d")
            if fecha_realizacion < today_date:
                msg = "La fecha de realización de la actividad no debe ser pasada!"
                self.add_error('fecha_realizacion', msg)
        else:
            msg = "La fecha es requerida"
            self.add_error('fecha_realizacion', msg)



        if hora_inicio:
            pass
        else:
            msg = "La hora es requerida"
            self.add_error('hora_inicio', msg)

        if hora_fin:
            pass
        else:
            msg = "La hora es requerida"
            self.add_error('hora_fin', msg)

        if hora_fin < hora_inicio:
            msg = "La hora de inicio debe ser inferior a la hora de finalización"
            self.add_error('hora_inicio', msg)

        if cupo_disponible <=0:
            msg = "El número de cupos de la actividad no puede ser cero!"
            self.add_error('cupo_disponible', msg)

        if cupo_disponible > self.instance.evento.numero_cupos:
            msg = "El número de cupos de la actividad no puede ser superior al número de cupos del evento!"
            self.add_error('cupo_disponible', msg)


class RegistroResultado(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        
        self.actividad_elegida = kwargs.pop('actividad_elegida', None)
        super(RegistroResultado, self).__init__(*args, **kwargs)
        
        self.fields['actividad'].initial = self.actividad_elegida
        self.fields['actividad'].widget = forms.HiddenInput()
        self.fields['descripcion'].label = 'Descripción'

    class Meta:
        model = Resultado
        fields = ('descripcion', 'actividad', )

class ModificacionResultado(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        
        self.actividad_elegida = kwargs.pop('actividad_elegida', None)
        super(ModificacionResultado, self).__init__(*args, **kwargs)
        
        self.fields['actividad'].initial = self.actividad_elegida
        self.fields['actividad'].widget = forms.HiddenInput()
        self.fields['descripcion'].label = 'Descripción'

    class Meta:
        model = Resultado
        fields = ('descripcion', 'actividad', )

    def clean(self):
        cleaned_data = super().clean()
        descripcion = cleaned_data.get('descripcion')
        