from django.db import models
from apps.eventos.models import Evento, Participante


def crear_ruta_actividad(instance, filename):
    return "fotos-actividad/%s-%s"%(instance.id, filename.encode('ascii','ignore'))

class Actividad(models.Model):
    ESTADOS = (('Activo','Activo'),
        ('Inactivo', 'Inactivo'),
    )
    nombre = models.CharField(max_length=200, verbose_name="Nombre", unique=True)  
    descripcion = models.CharField(max_length=300, verbose_name="Descripción") 
    lugar = models.CharField(max_length=150, verbose_name="Lugar")
    estado = models.CharField(choices=ESTADOS, verbose_name="Estado", default='Activo', max_length=50)
    fecha_realizacion = models.DateField(verbose_name="Fecha de realización",blank=False,null=False)
    hora_inicio = models.TimeField(verbose_name="Hora de inicio", blank=False,null=False)
    hora_fin = models.TimeField(verbose_name="Hora de finalización", blank=False,null=False)
    cupo_disponible = models.PositiveIntegerField(verbose_name="Cupo total")
    responsable_actividad = models.CharField(max_length=300, verbose_name="Responsable")
    evento = models.ForeignKey(Evento, related_name='actividades')
    participantes = models.ManyToManyField(Participante,related_name='actividades')
    imagen = models.ImageField(upload_to=crear_ruta_actividad, blank=True,help_text='Seleccione una imagen de perfil', verbose_name="Imagen")

    @staticmethod
    def revisar_existencia_actividad(actividad):
        try:
            actividad = Actividad.objects.get(id=actividad)
            return actividad
        except Exception:
            pass
        return False

    def is_active(self):
        if(self.estado == 'Activo'):
            return True
        else:
            return False

    def __str__(self):
        return "%s %s"%(self.nombre,self.descripcion)

    @staticmethod
    def obtener_nombre_actividad(nombre_actividad):
        try:
            actividad = Actividad.objects.get(nombre = nombre_actividad)
            return actividad
        except Exception as e:
            raise e
        return False

class Resultado(models.Model):
    ESTADOS = (('Activo','Activo'),
        ('Inactivo', 'Inactivo'),
    )

    descripcion = models.TextField(verbose_name="Descripción")
    estado = models.CharField(choices=ESTADOS, verbose_name="Estado", default='Activo', max_length=50)
    actividad = models.ForeignKey('Actividad')


        
