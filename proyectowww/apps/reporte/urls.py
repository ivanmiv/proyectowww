from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^asistencia_eventos_fecha$', porcentaje_participacion_fecha, name ='reporte_porcentaje_participacion_fecha'),
    url(r'^asistencia_eventos_fecha_datos$', porcentaje_participacion_fecha_datos, name ='reporte_porcentaje_participacion_fecha_datos'),
    url(r'^asistencia_actividades$', cantidad_participantes, name ='reporte_asistencia_actividades'),
    url(r'^asistencia_actividades_datos$', cantidad_participantes_datos, name ='reporte_asistencia_actividades_datos'),
]