import datetime

from django.shortcuts import render

# Create your views here.
from django.contrib import messages
from django.db.models.aggregates import Count
from django.db.models.functions.datetime import TruncDate
from django.http.response import JsonResponse
from django.shortcuts import render, redirect
from django.views.generic import ListView, TemplateView

from proyectowww.utilidades import verificar_cargo
from apps.eventos.models import *
from apps.actividad.models import Actividad


def obtener_porcentaje_participacion_fecha(fecha_inicio, fecha_fin, separacion="dia"):
    """
    a partir de 2 fechas calculamos el valor participantes/cupos de los eventos que tuvieron lugar entre esas dos fechas
    separar las fechas que se van a mostrar

    fecha = fecha_inicio
    while fecha < fecha_fin:
        fechas.add(fecha)
        fecha.day += 1

    1. obtener los eventos cuya fecha de inicio y fecha de fin se encuentra entre el rengo de fechas

    Eventos.objects.filter(fecha_inicio__in=[inicio, fin]....)


    2. sumar por dia , mes, o año los participantes de los eventos que completaron al inscripcion y los cupos

    for fecha in fechas:
        cupos = 0
        participantes= 0
        for evento in eventos
           if  evento . fecha_inicio <= fecha <= evento.fecha_fin:
                cupos += evento.cupos
                participantes += Participante.objects.filter(evento=evento,estado="Inscrito").count()


    3. obtener el porcentaje de asistencia a partir de eso dos datos


    :param fecha_inicio:
    :param fecha_fin:
    :param separacion = ""
    :return:
    """
    import random

    color_aleatorio = lambda: random.randint(0, 255)

    fechas = []
    conteo = []
    colores = []
    datos_tabla = []
    fechas_formateadas = []

    if type(fecha_inicio) == str or type(fecha_fin) == str:
        fecha_inicio = datetime.datetime.strptime(fecha_inicio, "%Y-%m-%d %H:%M:%S%z")
        fecha_fin = datetime.datetime.strptime(fecha_fin, "%Y-%m-%d %H:%M:%S%z")

    fecha_inicio_while = fecha_inicio
    while fecha_inicio_while < fecha_fin:
        fechas.append(fecha_inicio_while)
        fecha_inicio_while += datetime.timedelta(days=1)

    # Eventos que iniciaron y finalizaron dentro del rango de fechas
    eventos_rango_fechas = Evento.objects.annotate(inicio=TruncDate('fecha_inicio'), fin=TruncDate('fecha_fin')) \
        .filter(inicio__range=(fecha_inicio, fecha_fin), fin__range=(fecha_inicio, fecha_fin))

    for fecha in fechas:
        cupos = 0
        asistentes = 0
        #print("ASISTENTES", asistentes, "CUPOS", cupos)
        for evento in eventos_rango_fechas:
            if evento.fecha_inicio.date()<=fecha.date()<= evento.fecha_fin.date():
                cupos += evento.numero_cupos
                # Cantidad de inscritos que completaron la inscripcion asistiendo al evento
                asistentes += evento.participantes.filter(estado_inscripcion="INSCRITO").count()
                #print("EVENTO", evento.nombre, "ASISTENTES",
                #      evento.participantes.filter(estado_inscripcion="INSCRITO").count(), "CUPOS", evento.numero_cupos)

        porcentaje = (asistentes / cupos) * 100.0 if cupos > 0 else 0.0
        #print("ASISTENTES", asistentes, "CUPOS", cupos, "PORCENTAJE", porcentaje)
        porcentaje = float("%.2f" % porcentaje)
        conteo.append(porcentaje)
        colores.append('#%02X%02X%02X' % (color_aleatorio(), color_aleatorio(), color_aleatorio()))

        fecha_formateada = fecha.strftime('%d, %b %Y')
        datos_tabla.append({'0':fecha_formateada,'1':"%.2f%s"%(porcentaje,"%")})
        fechas_formateadas.append(fecha_formateada)


    data = {
        'etiquetas': fechas_formateadas,
        'valores': conteo,
        'colores': colores,
        'datos_tabla': datos_tabla,
        'ejeX': "Fechas",
        'ejeY': "Porcentaje de asistencia"
    }

    return data


@verificar_cargo(cargos_permitidos=["Gerente"])
def porcentaje_participacion_fecha_datos(request):
    datos_enviar = obtener_porcentaje_participacion_fecha(request.GET['fecha_inicio'], request.GET['fecha_fin'])

    return JsonResponse(datos_enviar)


@verificar_cargo(cargos_permitidos=["Gerente"])
def porcentaje_participacion_fecha(request):
    encabezados = ["Fecha", "Porcentaje de asistencia"]

    return render(request, "reporte_porcentaje_asistencia_fecha.html", {"encabezados": encabezados})


def cantidad_participantes(request):
    encabezados = ["Actividad", "Cantidad de asistentes"]
    eventos = Evento.objects.all()
    return render(request, "reporte_cantidad_asistentes.html", {"encabezados": encabezados, "eventos" : eventos})

def cantidad_participantes_datos(request):
    
    datos_enviar = obtener_cantidad_asistencia(request.GET['evento'])

    return JsonResponse(datos_enviar)

def obtener_cantidad_asistencia(_evento_):
    import random

    color_aleatorio = lambda: random.randint(0, 255)

    fechas = []
    conteo = []
    colores = []
    datos_tabla = []
    nombres_actividades = []

    evento_solicitado = Evento.objects.get(id = _evento_)
    actividades = Actividad.objects.filter(evento = evento_solicitado)

    for actividad in actividades:
        
        nombres_actividades.append(actividad.nombre)
        cantidad_participantes = actividad.participantes.count()

        conteo.append(cantidad_participantes)
        colores.append('#%02X%02X%02X' % (color_aleatorio(), color_aleatorio(), color_aleatorio()))
        datos_tabla.append({'0' : actividad.nombre, '1' : cantidad_participantes})

    if(len(conteo) == 0):
        asistencia_max = 0
    else:
        asistencia_max = max(conteo)
    print(asistencia_max)

    data = {
        'etiquetas': nombres_actividades,
        'valores': conteo,
        'colores': colores,
        'datos_tabla': datos_tabla,
        'maximo' : asistencia_max,
        'ejeX': "Actividades",
        'ejeY': "Cantidad de asistentes"
    }

    return data