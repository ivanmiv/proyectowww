from django.contrib import messages
from django.shortcuts import render, redirect

# Create your views here.

from django.http.response import JsonResponse
from django.urls import reverse_lazy
from django.views.generic import CreateView

from .forms import PagoForm
from .models import Pago

def inicio(request):
    return  redirect('pago_registrar')

def web_service_consultar_pago(request):
    respuesta = {}

    if request.method == "GET":
        try:
            numero_documento = request.GET['numero_documento']
            evento_id = request.GET['evento_id']
            valor = request.GET['valor']
            pago = Pago.objects.get(numero_documento_identificacion_cliente=numero_documento,evento_id=evento_id,monto=valor)
            respuesta['estado_pago'] = 'realizado'

        except Exception as e:
            print(e)
            respuesta['estado_pago'] = 'inexistente'
    else:
        respuesta = {'estado_pago':'inexistente'}

    return JsonResponse(respuesta)


class RegistrarPago(CreateView):
    model = Pago
    template_name = 'registrar_pago.html'
    form_class = PagoForm

    success_url = reverse_lazy('pago_registrar')


    def form_valid(self, form):
        messages.success(self.request, "Se ha registrado exitosamente el pago")
        return super(RegistrarPago, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, "Error, no se registró el pago, por favor verificar los datos")
        return super(RegistrarPago, self).form_invalid(form)