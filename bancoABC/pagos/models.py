from django.db import models

# Create your models here.


class Pago(models.Model):
    numero_documento_identificacion_cliente = models.CharField(max_length=20, verbose_name = 'número de documento del participante')
    monto = models.PositiveIntegerField(verbose_name='valor a pagar')
    evento_id = models.PositiveIntegerField(verbose_name='número identificador del evento')
    