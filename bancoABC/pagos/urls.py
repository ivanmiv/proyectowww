from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^consultar$', web_service_consultar_pago, name ="pago_consultar"),
    url(r'^registrar$', RegistrarPago.as_view(), name = "pago_registrar"),
    ]