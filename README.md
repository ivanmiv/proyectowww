# README #
Proyecto WWW


Instalación
===========
1. Instalar:

* python 3.6.2
* PostgreSQL 9.5

2. Instalar los requerimientos
    pip install -r requirements.txt

3. Descargar los archivos "settings.py" y "configuracionBD.json", los cuales deben estar ubicados en la carpeta proyectowww y raiz respectivamente. Recuerden modificar los valores de datosBD.json para los accesos a la base de datos.

4. Para cada aplicacion dentro de cada aplicacion de la carpeta "apps" deben crear una carpeta "migrations" y en ésta crear un archivo llamado \_\_init\_\_.py

5. Sincronizar la base de datos:
    python manage.py makemigrations
    python manage.py migrate
    
6. Ejecutar el servidor:
    python manage.py runserver
    
Nota: No olviden colocar en el archivo .gitignore  todos los archivos que no se requieren subir al repositorio al hacer un pullrequest (archivos como *.pyc  *~  secrets.json  etc)


Implementación

===========

1. Existe el archivo proyectowww/utilities.py, el cual contendrá funciones que podrían ser útiles en cualquier módulo.

2. Hacer funciones no superiores a 20 líneas de código en la medida de lo posible.

3. Realizar las validaciones por el lado de cliente y servidor.

4. Por convención realizar la identación a partir de espacios, no tabs.

5. En el nombre de las urls, procuremos mantener el estándar "nombreAplicacion_funcionalidad" (ejemplo: participante_datos_personales).

6. Uso de métodos pertienentes dentro de la definición de modelos.

7. Nombres de variables, métodos, clases, funciones y demás, que sean autoexplciativos, ej: cantidad_de_estudiantes_en_grupo = X

8. Nombres en singular para las aplicaciones de Django, ej: Usuario - Matricula

9. Es recomendable escribir el JavaScript en archivos e importarlo en las plantillas


Plantilla usada: https://wrapbootstrap.com/theme/nifty-responsive-admin-template-WB0048JF7